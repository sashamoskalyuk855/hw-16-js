// №1 - визначення в яку чверть потрапляє число
let min = Math.floor(Math.random() * 59)
if (min <= 15) {
    console.log(`I четверть`)
} else if (min <= 30) {
    console.log(`II четверть`)
} else if (min <= 45) {
    console.log(`III четверть`)
} else if (min <= 59) {
    console.log(`IV четверть`)
}


// №2 - висначення на якій мові виводити дні тижнів (1,2,3)
let ua = ["пн", "вт", "ср", "чт", "пт", "сб", "нд"]
let en = ["mo", "tu", "we", "th", "fr", "sa", "su"]
let arr = []

// (використання if) - №2.1
let lang = en
if (lang === ua) {
    arr = ua
    console.log(arr)
} else if (lang === en) {
    arr = en
    console.log(arr)
}

// (використання switch) - №2.2
switch (lang) {
    case ua:
        arr = ua
        console.log(arr)
        break
    case en:
        arr = en
        console.log(arr)
        break
    default:
        console.log(`Невірно вказане значення!`)
}

// (багатовимірні масиви) - №2.3
let trans = {
    ua: ["пн", "вт", "ср", "чт", "пт", "сб", `нд`],
    en: ["mo", "tu", "we", "th", "fr", "sa", "su"],
}
lang = `ua`
console.log(trans[lang])


// №3 - визначення суми перших та останній трьох чисел

function randomNumber (min, max) {
    min = Math.ceil(1000000);
    max = Math.floor(9999999);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
console.log(randomNumber())
let str = randomNumber().toString()
let first = +str[0] + +str[1] + +str[2]
console.log(first)
let last = +str[4] + +str[5] + +str[6]
console.log(last)

if (first === last) {
    console.log(`Щасливий`)
} else {
    console.log(`Вам не пощастило`)
}